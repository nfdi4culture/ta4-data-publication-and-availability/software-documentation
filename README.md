# Software Documentation

Research for Software Documentation - technical, tutorials, how-tos, cheatsheets, help docs, etc. The objective is to come up with recommendations for managing different types of documentation. The project is iterative and design research based so evaluation of documentation projects will be fed back into the recommendations.

If you have any suggestions for the research please raise a GitLab Issue or message simon.worthington@tib.eu 

Information on TIB Wiki: https://wiki.tib.eu/confluence/display/nfdi4culture/Software+Documentation+-+technical%2C+tutorials%2C+how-tos%2C+etc


## Style guides

- MDN Web Docs - Writing guidelines - https://developer.mozilla.org/en-US/docs/MDN/Writing_guidelines
- Mediawiki - https://www.mediawiki.org/wiki/Documentation/Style_guide


## Methods

- Diátaxis - https://diataxis.fr/


## Platforms / features

- Readthedocs


### Glossaries


## Resources

- OpenWebDocs - MDN contributors: https://openwebdocs.org/team/


## Documentation requirements (projects)

1. ADA Multi-format Publishing Pipeline
1. ADA Coputational Publishing Pipeline: Wikibase, Jupyter Notebooks, Quarto / Jupyter Books, CSS Paged Media, Git
1. Automated Literature Review (ALS) using pyami software with Jupyter Notebooks - https://github.com/semanticClimate/automated-literature-review
1. Editorial and Publishing Guide: 'tutorial and how-to' - for LIBER and NextGen Books client
1. CSS Paged Media Typesetting
1. Juypter Notebook - Software Citation deposit
1. Glossary creation in Wikidata and Wikibase


## Documentation created


## Questions


## Recomendations: Findings / Ideas




